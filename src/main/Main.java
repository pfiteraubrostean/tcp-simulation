package main;

import java.io.FileNotFoundException;
import java.io.IOException;

import sut.tcp.Simulation;
import util.Action;
import util.InputAction;
import util.OutputAction;

public class Main {

	public static Simulation simulation;
    public static void main(String[] args) throws Exception{
//        String hypFile = "hyp.dot";
//        String mapperFile = "freebsd";
//        if (args.length > 0) {
//            hypFile = args[0];
//            if (args.length > 1) {
//                mapperFile = args[1];
//            }
//        } 
//        
//        Automaton automaton = Dot.readDotFile(hypFile);
//        HandlerLoader loader = new HandlerLoader();
//        InvLangHandler handler = loader.loadMapper(mapperFile);
        
         
        
        reset();
        step(in("LISTEN"));
        step(in("ACCEPT"));
        int sutSeq = seq(step(in("SYN",1000,0,0)));
        step(in("ACK",1001,sutSeq+1,0));
        step(in("ACK+PSH",1001,sutSeq+1,1));
        step(in("ACK+PSH",1002,sutSeq+1,1));
        step(in("SEND"));
        step(in("SEND"));
    }
    
    private static InputAction in(String meth, Integer...args) {
        return new InputAction(meth, args);
    }
    
    private static void  reset() throws FileNotFoundException, IOException {
    	simulation = new Simulation();
    }
    
    private static OutputAction step(InputAction input) {
    	OutputAction output = (OutputAction) simulation.sendInput(input);
    	System.out.println(simulation.sendInput(input));
    	return output;
    }
    private static int seq(Action concreteAction) {
    	return concreteAction.getParam(0).getValue();
    }

}

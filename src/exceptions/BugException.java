package exceptions;

public class BugException extends DecoratedRuntimeException{
	private static final long serialVersionUID = 3349908291398696155L;

	public BugException(String message) {
		super(message);
	}
	
	public BugException(Exception exception) {
		super(exception.getMessage());
	}
}

package sut;

import java.io.IOException;

import sut.interfaces.SutInterface;

public interface SutProvider {
    public SutInterface newSut() throws IOException;
}

package sut.interfaces;

import java.util.List;

public interface Action {

	public String getMethodName();	
	public List<sut.interfaces.Parameter> getParams() ;
	
}

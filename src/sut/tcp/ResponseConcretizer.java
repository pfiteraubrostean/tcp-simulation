package sut.tcp;

import invlang.types.FlagSet;
import util.OutputAction;

public interface ResponseConcretizer {
    
    public OutputAction concretizeResponse(FlagSet flags, String absSeq,
            String absAck, int payloadLength);

}

package sut.tcp;

import invlang.mapperReader.InvLangHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import exceptions.BugException;

public class HandlerLoader {
    

    protected static final String DEFAULT_MAPPER_PATH = "input/mappers/";

    private String mapperPath;
    
    public HandlerLoader() throws IOException{
        this(DEFAULT_MAPPER_PATH);
    }
    
    public HandlerLoader(String folder) throws IOException{
        this.mapperPath = folder;
        if (!new File(this.mapperPath).isDirectory()) {
            throw new BugException(folder + " should be dir");
        }
    } 
    
    public InvLangHandler loadMapper( String mapperName) throws IOException{
        InvLangHandler handler = null;
        System.out.println("Reading mapper file...");
        try (BufferedReader input = new BufferedReader(new FileReader(new File(this.mapperPath + File.separator + mapperName)))) {
            int c;
            StringBuilder sb = new StringBuilder();
            while ((c = input.read()) != -1) {
                sb.append((char) c);
            }
            System.out.println("Transforming mapper...");
            handler = new InvLangHandler(sb.toString(), null);
            //handler = new InvLangHandler(sb.toString(), new Reducer(Reducer.RANGE_LENGTH+4, 2, Reducer.INITIAL_START, Reducer.NR_RANGES+2));
        }
        System.out.println("Finished transforming mapper...");
        return handler;
    }
}

package sut.tcp;

import invlang.types.FlagSet;

public interface ConcretizationInterface{
    public String processIncomingRequest(FlagSet flags, Integer concSeq,
            Integer concAck, int payloadLength);

    public  String processOutgoingResponse(FlagSet flags, String absSeq,
            String absAck, int payloadLength);
    public void reset();
}

package sut.tcp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import sut.tcp.InvlangMapper.Inputs;
import sut.tcp.InvlangMapper.Mappings;
import util.Calculator;
import util.OutputAction;
import exceptions.BugException;
import invlang.mapperReader.InvLangHandler;
import invlang.types.EnumValue;
import invlang.types.FlagSet;

/**
 * Obtains a valid concretization for requests/ responses by trying out
 * random related values. It picks the first configuration which maps to the given abstractions
 * for both sequence and acknowledgement numbers. In case none of them do, returns null.
 * 
 * This is used as an alternative to executeInverted called on the handler.
 */
public class RandomResponseConcretizer implements ResponseConcretizer{
    private InvLangHandler handler;
    public RandomResponseConcretizer(InvLangHandler handler) {
        this.handler = handler;
    }
    
    

	@Override
    public OutputAction concretizeResponse(FlagSet flags, String absSeq,
            String absAck, int payloadLength) {
    	Configuration freshConfiguration = generateFreshConfiguration();
    	List<Integer> pointsOfInterest = getPointsOfInterest(15);
    	//List<Configuration> configurations = generateConfigurations(freshConfiguration, pointsOfInterest);
    	Configuration foundConfig = getFirstConfigurationMatchingAbstractions(freshConfiguration, pointsOfInterest, 
    			flags, absSeq, absAck, payloadLength);
        
        if (foundConfig == null) {
    		throw new BugException("Cannot concretize the output " + flags + "(" + absSeq + "," + absAck + "," + payloadLength + ")" 
            		+ " for the mapper:\n" + handler.getState() + 
                    "\nhaving checked these points of interest " + pointsOfInterest
                    + "\n" + Arrays.asList(Thread.currentThread().getStackTrace()));
//        	return null;
        } else {
            updateMapperWithConcretization(flags, foundConfig.seqNum, foundConfig.ackNum, payloadLength);
            OutputAction output = new OutputAction(flags.toString(), new Integer [] {foundConfig.seqNum, foundConfig.ackNum, payloadLength});
            return output;
        }
    }
    
    private Configuration generateFreshConfiguration() {
    	 Integer lastLearnedSeqInt = (Integer) handler.getState().get("lastLearnerSeq");
         Integer concSeq;
         
         if (lastLearnedSeqInt == null || lastLearnedSeqInt == InvlangMapper.NOT_SET) {
             concSeq = (int) Calculator.randWithinRange(1000L, 0xffffL);
         } else {
             concSeq = (int) Calculator.sum(lastLearnedSeqInt, Calculator.randWithinRange(70000,100000));
         }
         Integer concAck = (int) Calculator.randWithinRange(1000L, 0xffffL);
         
         // since we are only playing with integers (and not long), make sure they are positive
         if (concSeq < 0) {
        	 concSeq *=(-1);
         }
         if (concAck < 0) {
        	 concAck *=(-1);
         }
         
         return new Configuration(concSeq, concAck);
    }
    
    private Configuration getFirstConfigurationMatchingAbstractions(
    		Configuration fresh, List<Integer> pointsOfInterest, FlagSet flags, String absSeq, String absAck, int payloadLength) {
    	if (checkIfValidConcretization(flags, absSeq, fresh.seqNum, absAck, fresh.ackNum, payloadLength) ){
    		return fresh;
    	} else {
    		for (Integer possibleAck : pointsOfInterest) {
    			if (checkIfValidConcretization(flags, absSeq, fresh.seqNum, absAck, possibleAck, payloadLength) ){
    				return new Configuration(fresh.seqNum, possibleAck);
    			}
    		}
    		for (Integer possibleSeq : pointsOfInterest) {
    			if (checkIfValidConcretization(flags, absSeq, possibleSeq, absAck, fresh.ackNum, payloadLength) ){
    				return new Configuration(possibleSeq, fresh.ackNum);
    			}
    		}
    		
    		for (Integer possibleSeq : pointsOfInterest) {
    			for (Integer possibleAck : pointsOfInterest) {
    				if (checkIfValidConcretization(flags, absSeq, possibleSeq, absAck, possibleAck, payloadLength) ){
        				return new Configuration(possibleSeq, possibleAck);
        			}	
    			}
    		}
    		
    	}
    	
    	return null;
    }
    
    /**
     * Generates an ordered list of sequence and acknowledgement number configurations from a freshSeq, freshAck and pointsOfInterest.
     * Configurations using fresh values appear before those only made from points of interest. In a later version, these fresh
     * values will be included in the points of interest, and not treated differently.
     * 
     * The advantage of storing to a list is that you can shuffle it (for bettter testing).
     */
    private List<Configuration> generateConfigurations(Configuration freshConfiguration, List<Integer> pointsOfInterest) {
        List<Configuration> seqAckConfigurations = new LinkedList<>();
        seqAckConfigurations.add(freshConfiguration);
        pointsOfInterest.forEach(possibleAck -> seqAckConfigurations.add(new Configuration(freshConfiguration.seqNum, possibleAck)));
        pointsOfInterest.forEach(possibleSeq -> seqAckConfigurations.add(new Configuration(possibleSeq, freshConfiguration.ackNum)));
        for (Integer possibleSeq : pointsOfInterest) {
            for (Integer possibleAck : pointsOfInterest) {
                seqAckConfigurations.add(new Configuration(possibleSeq, possibleAck));
            }
        }
        return seqAckConfigurations;
    } 
    
    /**
     * Configuration of concrete sequence and acknowledgement numbers.
     */
    static class Configuration {
        public final Integer seqNum;
        public Configuration(Integer seqNum, Integer ackNum) {
            super();
            this.seqNum = seqNum;
            this.ackNum = ackNum;
        }
        public final Integer ackNum;
        
        public String toString() {
        	return "seq:" + seqNum + " ack:" + ackNum;
        }
    }
    
    private List<Integer> getPointsOfInterest(int numberOfIncrements) {
        List<Integer> valuesOfInterest = new ArrayList<Integer>();

        for (Entry<String, Object> entry : this.handler.getState().entrySet()) {
            if (entry.getValue() instanceof Integer) {
                int value = (Integer) entry.getValue();
                if (value != InvlangMapper.NOT_SET && !valuesOfInterest.contains(value)) {
                    for (int i=0; i < numberOfIncrements; i ++) {
                        if (!valuesOfInterest.contains(value+i)) {
                            valuesOfInterest.add(value+i);
                        }
                    }
                }
            }
        }
        valuesOfInterest.add(0);
        
        return valuesOfInterest;
    }
    
    
    
    
    private boolean checkIfValidConcretization(FlagSet flags, String absSeq, int concSeq,
            String absAck, int concAck, int payloadLength) {
    	// RESPONSE
        handler.setFlags(Inputs.FLAGS, flags);
        handler.setInt(Inputs.CONC_SEQ, concSeq);
        handler.setInt(Inputs.CONC_ACK, concAck);
        handler.setInt(Inputs.CONC_DATA, payloadLength);
        handler.execute(Mappings.INCOMING_RESPONSE, false);
        EnumValue resultingAbsSeq = handler.getEnumResult(Inputs.ABS_SEQ);
        EnumValue resultingAbsAck = handler.getEnumResult(Inputs.ABS_ACK);
        return resultingAbsSeq.getValue().equals(absSeq)
                && resultingAbsAck.getValue().equals(absAck);
        
//        // REQUEST
//        handler.setFlags(Outputs.FLAGS_OUT, flags);
//        handler.setInt(Outputs.CONC_SEQ, concSeq);
//        handler.setInt(Outputs.CONC_ACK, concAck);
//        handler.setInt(Outputs.CONC_DATA, payloadLength);
//       	handler.execute(Mappings.OUTGOING_REQUEST, false);
//        EnumValue resultingAbsSeq = handler.getEnumResult(Outputs.ABS_SEQ);
//        EnumValue resultingAbsAck = handler.getEnumResult(Outputs.ABS_ACK);
//        return resultingAbsSeq.getValue().equals(absSeq)
//                && resultingAbsAck.getValue().equals(absAck);
    }
    
    private void updateMapperWithConcretization(FlagSet flags, int concSeq, int concAck, int payloadLength) {
    	// RESPONSE
        handler.setFlags(Inputs.FLAGS, flags);
        handler.setInt(Inputs.CONC_SEQ, concSeq);
        handler.setInt(Inputs.CONC_ACK, concAck);
        handler.setInt(Inputs.CONC_DATA, payloadLength);
        handler.execute(Mappings.INCOMING_RESPONSE);
        
//    	// REQUEST
//        handler.setFlags(Outputs.FLAGS_OUT, flags);
//        handler.setInt(Outputs.CONC_SEQ, concSeq);
//        handler.setInt(Outputs.CONC_ACK, concAck);
//        handler.setInt(Outputs.CONC_DATA, payloadLength);
//        handler.execute(Mappings.OUTGOING_REQUEST);
    }

    
    
//  private List<Long> getPointsOfInterest() {
//  List<Long> valuesOfInterest = new ArrayList<Long>();
//
//  for (Entry<String, Object> entry : this.handler.getState().entrySet()) {
//      if (entry.getValue() instanceof Integer) {
//          int value = (Integer) entry.getValue();
//          if (value != InvlangMapper.NOT_SET && !valuesOfInterest.contains((long)value)) {
//              long longOfVal = InvlangMapper.getUnsignedInt(value);
//              for (int i=0; i < 2; i ++) {
//                  if (!valuesOfInterest.contains(longOfVal+i)) {
//                      valuesOfInterest.add((long)(longOfVal+i));
//                  }
//              }
//          }
//      }
//  }
//  valuesOfInterest.add(0L);
//  
//  return valuesOfInterest;
//}

}

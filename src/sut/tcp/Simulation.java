package sut.tcp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import de.ls5.jlearn.interfaces.Automaton;
import de.ls5.jlearn.interfaces.State;
import de.ls5.jlearn.interfaces.Symbol;
import de.ls5.jlearn.shared.SymbolImpl;
import exceptions.BugException;
import exceptions.DecoratedRuntimeException;
import invlang.types.FlagSet;
import sut.interfaces.SutInterface;
import util.Action;
import util.InputAction;
import util.OutputAction;
import util.Parameter;

public class Simulation implements SutInterface{
	private static boolean verbose = false;
	private static String PROPERTIES_FILE = "simulation.properties";
			
    private Automaton model;
    private ConcretizationInterface mapper;
    private List<Action> trace;
    private State currentState;
    private PrintWriter pw;

    public Simulation(Automaton hyp, ConcretizationInterface mapper) {
        this.model = hyp;
        this.mapper = mapper;
        this.trace = new ArrayList<Action>();
        if (verbose) {
	        try {
				this.pw = new PrintWriter(new File("sim_log.txt"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.exit(0);
			}
        }
    }
    
    public Simulation(String modelFile, String mapperFile) throws IOException {
	    init(modelFile, mapperFile);
    }
    
    // create easily instantiated constructor
    public Simulation() throws FileNotFoundException, IOException {
    	Properties simProperties = new Properties();
    	simProperties.load(new FileInputStream(PROPERTIES_FILE));
    	String modelFile = getProperty(simProperties, "model");
    	String mapperFile = getProperty(simProperties, "mapper");
    	init(modelFile, mapperFile);
    }
    
    private String getProperty(Properties propFile, String prop) {
    	if (propFile.containsKey(prop)) {
    		return propFile.getProperty(prop);
    	} else throw new BugException("Missing property " + prop);
    }
    
    private void init(String modelFile, String mapperFile) throws IOException  {
    	ModelLoader modelLoader = new ModelLoader();
		Automaton model = modelLoader.loadModel(modelFile);
	    HandlerLoader loader = new HandlerLoader();
	    ConcretizingMapper mapper = new ConcretizingMapper(loader.loadMapper(mapperFile));
	    init(model, mapper);
    }
    
    private void init(Automaton hyp, ConcretizationInterface mapper) {
    	this.model = hyp;
        this.mapper = mapper;
        this.trace = new ArrayList<Action>();
        this.currentState = model.getStart();
        if (verbose) {
	        try {
				this.pw = new PrintWriter(new File("sim_log.txt"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.exit(0);
			}
        }
    }
    
    private void out(String output) {
    	if (verbose)
    		pw.println(output);
    }

    public OutputAction processSymbol(InputAction methodWrapper) {
    	trace.add(methodWrapper);
    	try {
        String requestString = null;
        
        out("Sim received: " + methodWrapper.getValuesAsString());
        
        // packet
        if (methodWrapper.getParameters().size() > 0)  {
            requestString = this.mapper.processIncomingRequest(new FlagSet(methodWrapper.getMethodName()), 
                    methodWrapper.getParam(0).getValue(), methodWrapper.getParam(1).getValue(), methodWrapper.getParam(2).getValue());
            // hack needed to preserve the order of flags
            int startOfParan = requestString.indexOf('(');
            requestString = methodWrapper.getMethodName() + requestString.substring(startOfParan, requestString.length());
        // action
        } else {
            requestString = methodWrapper.getMethodName();
        }
        out(this.mapper.toString());
        Symbol inputSymbol = new SymbolImpl(requestString);
        
        Symbol outputSymbol = currentState.getTransitionOutput(inputSymbol);

        OutputAction output = null;
        
        if (outputSymbol == null) {
            out("undefined input: " + requestString);
            output =  new OutputAction("UNDEFINED");
        } else {
            this.currentState = currentState.getTransitionState(inputSymbol);
            if (outputSymbol.toString().contains("(")) {
                Packet packet = new Packet(outputSymbol.toString());
                String response = this.mapper.processOutgoingResponse(packet.flags, packet.seq.toString(), packet.ack.toString(), packet.payloadLength);
                if (response != null) {
                	output = new OutputAction(""+response);
                } else {
                	output = new OutputAction("FAIL");
                }
            } else {
                String formattedOutput = outputSymbol.toString().replaceAll("\\(|\\)", "_");
                output = new OutputAction(formattedOutput);
            }
        }
        out(this.mapper.toString());
        out("Sim responded: " + output.getValuesAsString());
        trace.add(output);
        return output;
    	}catch(DecoratedRuntimeException exc) {
        	exc.addDecoration("trace:", trace);
        	throw exc;
        }
    }

    public void reset() {
        this.mapper.reset();
        this.currentState = model.getStart();
        trace.clear();
        out("Sim reset");
    }

	@Override
	public sut.interfaces.OutputAction sendInput(sut.interfaces.InputAction inputAction) {
		//tomte input to sim input
		InputAction input = tomteInpToSimInp(inputAction);
		OutputAction output = this.processSymbol(input);
		
		// sim output to tomte output without payload parameter
		return simOutToTomteOut(output);
	}

	@Override
	public void sendReset() {
		this.reset();
	}
	
    private InputAction tomteInpToSimInp(sut.interfaces.InputAction tomteInp) {
    	String methodName = tomteInp.getMethodName();
    	if (methodName.startsWith("I")) {
    		methodName = methodName.substring(1);
    	}
    	List<sut.interfaces.Parameter> params = new ArrayList<sut.interfaces.Parameter>();
    	params.addAll(tomteInp.getParams());
    	if (params.size() > 1) {
    		int payload = methodName.contains("PSH")? 1: 0;
    		params.add(new Parameter(payload, 2));
    	}
    	return new InputAction(methodName, params);
    }
    
    private sut.interfaces.OutputAction simOutToTomteOut(OutputAction simOutput) {
    	List<sut.interfaces.Parameter> params = new ArrayList<>(simOutput.getParameters());
		if (params.size() == 3) 
			params.remove(params.size()-1);
		OutputAction tomteOutput = new OutputAction("O"+simOutput.getMethodName(), params);
		return tomteOutput;
    }
}

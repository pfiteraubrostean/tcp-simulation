package sut.tcp;

import java.util.Arrays;

import exceptions.BugException;
import invlang.mapperReader.InvLangHandler;
import invlang.types.EnumValue;
import invlang.types.FlagSet;
import sut.tcp.InvlangMapper.Inputs;
import sut.tcp.InvlangMapper.Mappings;
import sut.tcp.InvlangMapper.Outputs;
import util.OutputAction;

public class ConcretizingMapper implements ConcretizationInterface{
    private InvLangHandler handler;
    private ResponseConcretizer concretizer;

    public ConcretizingMapper(InvLangHandler handler) {
        this.handler = handler;
        this.concretizer = new InvlangConcretizer(handler);
    }
    
    
//    MAP outgoingRequest (int concSeqOut, int concAckOut, flags flagsOut, int concDataOut
//            -> absin absSeqOut, absin absAckOut, flags flagsOut2, int absDataOut)
    
    public String processIncomingRequest(FlagSet flags, Integer seqNr, Integer ackNr, int payloadLength) {
        handler.setFlags(Outputs.FLAGS_OUT, flags);
        handler.setInt(Outputs.CONC_DATA, payloadLength);
        handler.setInt(Outputs.CONC_SEQ, (int) seqNr);
        handler.setInt(Outputs.CONC_ACK, (int) ackNr);
        handler.execute(Mappings.OUTGOING_REQUEST, true);
        EnumValue absSeq = handler.getEnumResult(Outputs.ABS_SEQ);
        EnumValue absAck = handler.getEnumResult(Outputs.ABS_ACK);
        return Serializer.abstractMessageToString(flags, absSeq.getValue(), absAck.getValue(), payloadLength);
    }
    
    
//    MAP incomingResponse (flags flagsIn, int concSeqIn, int concAckIn, int concDataIn
//            -> abssut absSeqIn, abslearner absAckIn)
    public String processOutgoingResponse(FlagSet flags, String absSeq, String absAck, int payloadLength) {
    	OutputAction concreteOutput =  concretizer.concretizeResponse(flags, absSeq, absAck, payloadLength);
    	if (concreteOutput != null)
    		return concreteOutput.getValuesAsString();
    	else {
    		return null;
    	}
    }
    
    
    /**
     * Reads an int (which is always signed in java) as unsigned,
     * stored in a long
     * @param x
     * @return
     */
    protected static long getUnsignedInt(int x) {
        return x & 0x00000000ffffffffL;
    }
    
    public void reset() {
        this.handler.reset();
    }
    
    public String toString() {
        return "Handler state: " + this.handler.getState();
    }
}

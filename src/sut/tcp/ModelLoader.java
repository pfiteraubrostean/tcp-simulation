package sut.tcp;

import java.io.File;

import de.ls5.jlearn.interfaces.Automaton;
import util.learnlib.Dot;

public class ModelLoader {
	protected static final String DEFAULT_MODEL_PATH = "input/models/";
	private final String modelsDir;
	
	public ModelLoader(String modelsDir) {
		this.modelsDir = modelsDir;
	}
	
	public ModelLoader() {
		this(DEFAULT_MODEL_PATH);
	}
	
	public Automaton loadModel(String modelName) {
		return Dot.readDotFile(this.modelsDir + File.separator + modelName);
	}
}

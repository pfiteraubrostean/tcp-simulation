package sut.tcp;

import exceptions.BugException;
import invlang.mapperReader.InvLangHandler;
import invlang.types.FlagSet;
import sut.tcp.InvlangMapper.Inputs;
import sut.tcp.InvlangMapper.Mappings;
import sut.tcp.InvlangMapper.Outputs;
import util.OutputAction;

public class InvlangConcretizer implements ResponseConcretizer{
    private InvLangHandler handler;
    private RandomResponseConcretizer fallback;
    public InvlangConcretizer(InvLangHandler handler) {
        this.handler = handler;
        this.fallback = new RandomResponseConcretizer(handler); 
    }

	@Override
	public OutputAction concretizeResponse(FlagSet flags, String absSeq, String absAck, int payloadLength) {
      handler.setEnum(Inputs.ABS_SEQ, "abssut", absSeq);
      handler.setEnum(Inputs.ABS_ACK, "abslearner", absAck);
   //   handler.setInt(Outputs.ABS_DATA, payloadLength);
      handler.executeInverted(Mappings.INCOMING_RESPONSE);

      if (handler.hasResult()) {
          int concSeq = handler.getIntResult(Inputs.CONC_SEQ);
          int concAck = handler.getIntResult(Inputs.CONC_ACK);
          System.out.println(concSeq + " " + concAck);
          OutputAction output = new OutputAction(flags.toString(), new Integer [] {concSeq, concAck, payloadLength});
          return output;
      } else {
    	  OutputAction output = this.fallback.concretizeResponse(flags, absSeq, absAck, payloadLength);
    	  if (output != null) {
    		  throw new BugException("This one works");
    	  }
    	  
    	  throw new BugException("Couldn't concretize " + handler.getState());
      }
	}

}

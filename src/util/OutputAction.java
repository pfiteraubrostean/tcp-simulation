package util;

import java.util.List;

public class OutputAction extends Action implements sut.interfaces.OutputAction{

	public OutputAction(String methodName, List<sut.interfaces.Parameter> parameters) {
		super(methodName, parameters);
	}
	
	public OutputAction(String action) {
		super(action);
	}
	
	public OutputAction(String methodName, Integer [] parameters) {
	    super(methodName, parameters);
	}
}

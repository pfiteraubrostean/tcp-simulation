package util;

import java.util.List;

public class InputAction extends Action implements sut.interfaces.InputAction{

	public InputAction(String methodName, List<sut.interfaces.Parameter> parameters) {
		super(methodName, parameters);
	}

//	public InputAction(Action action) {
//		super(action);
//	}
//	
	public InputAction(String action) {
		super(action);
	}
	
	public InputAction(String methodName, Integer [] parameters) {
        super(methodName, parameters);
    }

}
